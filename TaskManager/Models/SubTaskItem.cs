﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Models
{
    public class SubTaskItem
    {
        public int sub_task_id { get; set; }
        public string description { get; set; }
        public int parent_task_id { get; set; }
    }
}
