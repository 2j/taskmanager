﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Models
{
    [DynamoDBTable("Tasks")]
    public class TaskItem
    {
        [DynamoDBHashKey]
        public Guid task_id { get; set; }
        public TaskState state { get; set; }
        public string description { get; set; }
        public string long_description { get; set; }
        public IEnumerable<string> subtasks { get; set; }
        public string priority { get; set; }
        public DateTime date_due { get; set; }
    }

    public enum TaskState
    {
        Todo,
        InProgress,
        Done,
        Cancelled,
    };
}
