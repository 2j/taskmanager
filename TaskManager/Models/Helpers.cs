﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.Runtime;

namespace TaskManager.Models
{
    public static class Helpers
    {
        // const string ACCESS_KEY = ;
        // const string SECRET_KEY = ;
        const string TABLE_NAME = "Tasks";

        public static AmazonDynamoDBClient GetDynamoDbClient()
        {
            var cred = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
            var region = RegionEndpoint.USEast2;

            return new AmazonDynamoDBClient(cred, region);
        }
            


    }
}
