using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TaskManager.Models;
using Amazon.DynamoDBv2.DocumentModel;

namespace TaskManager.Pages
{
    public class EditTaskModel : PageModel
    {
        public TaskItem EditTask { get; set; }

        public async Task OnGetAsync(Guid task_id)
        {
            EditTask = await GetEditTask(task_id);
        }

        public async Task<IActionResult> OnPostAsync(TaskItem task)
        {
            var client = Helpers.GetDynamoDbClient();
            var ctx = new DynamoDBContext(client);
            await ctx.SaveAsync(task);

            return RedirectToPage("Index");
        }

        private async Task<TaskItem> GetEditTask(Guid task_id)
        {
            var client = Helpers.GetDynamoDbClient();
            var ctx = new DynamoDBContext(client);

            return await ctx.LoadAsync<TaskItem>(task_id);
        }
    }
}