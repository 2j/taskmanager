﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TaskManager.Models;
using Amazon.DynamoDBv2.DataModel;

namespace TaskManager.Pages
{
    public class IndexModel : PageModel
    {
        public TaskItem Task { get; set; }
        public IEnumerable<TaskItem> Tasks { get; set; }

        public async Task OnGetAsync()
        {
            var client = Helpers.GetDynamoDbClient();
            List<ScanCondition> conditions = new List<ScanCondition>();
            DynamoDBContext ctx = new DynamoDBContext(client);
            Tasks = await ctx.ScanAsync<TaskItem>(conditions).GetRemainingAsync();
        }

        public async Task<IActionResult> OnPostAddAsync(TaskItem task)
        {
            task.task_id = Guid.NewGuid();

            var client = Helpers.GetDynamoDbClient();
            DynamoDBContext ctx = new DynamoDBContext(client);
            await ctx.SaveAsync(task);

            return RedirectToAction(Request.ToString());
        }

        public IActionResult OnPostEdit(Guid task_id)
        {
            return RedirectToPage("EditTask", new { task_id });
        }

        public async Task<IActionResult> OnPostRemove(Guid task_id)
        {
            var client = Helpers.GetDynamoDbClient();
            DynamoDBContext ctx = new DynamoDBContext(client);
            await ctx.DeleteAsync<TaskItem>(task_id);

            return RedirectToAction(Request.ToString());
        }
    }
}
